import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import HomeCarousel from './HomeCarousel';
import HomeWorkoutCard from './HomeWorkoutCard';
import HomePricePlans from './HomePricePlans';
import Footer from './Footer';
import Header from './Header';



function Home() {

  return (
    <>
      <div className='home-backcolor'>
        <Header />
        <HomeCarousel />
        <HomeWorkoutCard />
        <HomePricePlans />
      </div >
      <Footer />
    </>
  )
}

export default Home

