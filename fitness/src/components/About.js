import React from 'react'
import './About.css'
import { FaSearch } from "react-icons/fa";
import Footer from './Footer'
import Header from './Header';

function About() {
  return (
    <>
      <Header />
      <div className='cont'>
        <div className='about-search-container'>
          <h1 className='about-search-heading'>How can we help you today?</h1>
          <div className='about-search-input-flex'>
            <input type="text" name="search" id="#search" placeholder='Enter your search term here...' className='about-search-input' />
            <FaSearch className='about-searth-icon' />
          </div>
        </div>
        <div className='about-fitness-club-conatiner'>
          <h1>About fitness club</h1>
          <div className='about-card-container'>
            <div className='about-card'>
              <h4>What is fitness club?</h4>
              <p>At fitness club, we make fitness fun and easy. We have best-in-class trainers & offer group workouts ranging from yoga to Boxing. Our workouts can be done both at a fitness club center and at home with the help of do it yourself workout videos. fitnessclub.fit uses the best in technology to give you a world-class experience. You can book classes, follow workout videos - all with the click of a button from the fitnessclub.fit or website. </p>
            </div>
            <div className='about-card'>
              <h4>What kind of workout are available at fitness club?</h4>
              <p>At fitness club centers, you will find a wide variety of workout formats, such as - Boxing, Dance Fitness, Yoga, S&C. These workouts focus on strength, muscle endurance, flexibility, stamina & balance. So mix it up, pick formats that help you achieve your personal fitness goals.You will also find unique workout formats designed by experienced trainers.</p>
            </div>
          </div>
          <div className='about-card-container'>
            <div className='about-card'>
              <h4>How is the fitness center different from a regular gym?</h4>
              <p>With our non-conventional training facility, we aim to give you a balanced mix of workouts that will enhance your health while having fun at the same time. fitness club differentiates itself, from other fitness centers by offering group workouts that focus on overall development. fitness club has a simple philosophy - make fitness fun and easy with the help of best-in-class trainers and group workouts.  </p>
            </div>
            <div className='about-card'>
              <h4>Which fitness services does fitnessclub.fit offer?</h4>
              <p>This is a unique service offered by fitness club. They are trainer-led sessions in various different formats ranging from cardio based dance fitness to muscle building strength & conditioning and relaxing yoga and stretching. This is a novel way to work out your whole body with others like you giving you motivation. </p>
            </div>
          </div>
          <div className='about-card-container'>
            <div className='about-card'>
              <h4>Can I buy now and start later?</h4>
              <p>Yes, you can select a different date to start your pack at the time of purchasing the pack. You can extend this date further even after pack purchase. However, you must do this before your first fitness club class or check-in. The extension is allowed only till 30 days of your originally selected pack purchase date and you can make this change twice. In case of session based packs like fitness club transform and PT, the start date is determined by the day of your first session. </p>
            </div>
            <div className='about-card'>
              <h4>Which pack should I buy?</h4>
              <p>fitness club offers a variety of all-access passes tailored to your needs. Amongst our fitness options, fitness club pass Elite, Pro and At-home are the most popular. In addition to these, you can also buy sessions with our weight loss coaches under our Transform program.</p>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  )
}

export default About
