import React, { useState } from 'react';
import Header from './Header';
import Footer from './Footer';
import './bmi.css'

const BMICalculator = () => {
  const [weight, setWeight] = useState('');
  const [height, setHeight] = useState('');
  const [bmi, setBMI] = useState(null);
  const [weightToReduce, setWeightToReduce] = useState(null);
  const [message, setMessage] = useState('');

  const calculateBMI = () => {
    if (weight && height) {
      const heightInMeters = height / 100; // Convert height to meters
      const bmiValue = (weight / (heightInMeters * heightInMeters)).toFixed(2);
      setBMI(bmiValue);

      // Calculate how much weight to reduce to reach a BMI of 25 (as an example target)
      const targetBMI = 25;
      const targetWeight = targetBMI * (heightInMeters * heightInMeters);
      const weightToReduceValue = weight - targetWeight;
      setWeightToReduce(weightToReduceValue > 0 ? weightToReduceValue.toFixed(2) : null);

      // Determine if BMI is within the normal range (18.5 to 24.9)
      if (bmiValue >= 18.5 && bmiValue <= 24.9) {
        setMessage('Perfect! Your BMI is within the normal range.');
      } else {
        setMessage('Your BMI is outside the normal range.');
      }
    } else {
      setBMI(null);
      setWeightToReduce(null);
      setMessage('');
    }
  };

  const clearInputs = () => {
    setWeight('');
    setHeight('');
    setBMI(null);
    setWeightToReduce(null);
    setMessage('');
  };

  return (
    <div>
      <Header />
      <div className='bmi-container'>
        <div className="about-BMI">
          <h1>Understanding Body Mass Index (BMI)</h1>

          <p>
            Body Mass Index (BMI) is a numerical value of a person's weight in relation to their height. It is a simple and widely used indicator of body fatness and helps assess health risks associated with weight.
          </p>

          <h2>How to Calculate BMI?</h2>
          <p>
            BMI is calculated by dividing a person's weight in kilograms by the square of their height in meters. The formula is: <strong>BMI = weight (kg) / (height (m) * height (m))</strong>.
          </p>

          <h2>Interpreting BMI Results</h2>
          <ul>
            <li><strong>Underweight:</strong> BMI less than 18.5</li>
            <li><strong>Normal weight:</strong> BMI 18.5 to 24.9</li>
            <li><strong>Overweight:</strong> BMI 25 to 29.9</li>
            <li><strong>Obesity:</strong> BMI 30 or greater</li>
          </ul>

          <h2>Healthy BMI Range</h2>
          <p>
            The ideal BMI falls within the range of 18.5 to 24.9. This range is associated with lower health risks.
          </p>

          <h2>Using the BMI Calculator</h2>
          <p>
            Utilize the BMI calculator to input your weight and height, and it will provide your BMI along with recommendations for a healthy weight range.
          </p>

          <p>
            Remember, BMI is a useful screening tool, but it may not account for all factors influencing health, such as muscle mass. Consult with a healthcare professional for personalized advice.
          </p>
        </div>
        <div className='bmi-calculator-container'>
          <div className="bmi-calculator">
            <div className="input-container">
              <label htmlFor="weight">Weight (kg): </label>
              <input
                type="number"
                id="weight"
                value={weight}
                onChange={(e) => setWeight(e.target.value)}
              />
            </div>

            <div className="input-container">
              <label htmlFor="height">Height (cm): </label>
              <input
                type="number"
                id="height"
                value={height}
                onChange={(e) => setHeight(e.target.value)}
              />
            </div>

            <div className="button-container">
              <button className="calculate-btn" onClick={calculateBMI}>
                Calculate BMI
              </button>
              <button className="clear-btn" onClick={clearInputs}>
                Clear
              </button>
            </div>

            {bmi !== null && (
              <div className="result-container">
                <h3>Your BMI: {bmi}</h3>
                {message && <p className={message.includes('Perfect') ? 'perfect-text' : 'reduce-text'}>{message}</p>}
                {weightToReduce !== null && (
                  <p className="reduce-text">Reduce {weightToReduce} kg to reach a BMI of 25.</p>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default BMICalculator;