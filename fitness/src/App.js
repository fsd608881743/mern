import './App.css';
import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from './components/Login';
import Signup from './components/Signup';
import Home from './components/Home';
import Sports from './components/Sports';
import NotFound from './PageNotFound'
import About from './components/About';
import Contact from './components/Contact'
import Cart from './components/Cart'
import BMICalculator from './components/BMICalculator';



library.add(fas);

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route exact path='/' element={<Login />}></Route>
          <Route exact path='/home' element={<Home />}></Route>
          <Route exact path='/signup' element={<Signup />}></Route>
          <Route exact path='/about' element={<About />}></Route>
          <Route exact path='/sports' element={<Sports />}></Route>
          <Route path="*" element={<NotFound />} />
          <Route path="/login" element={<Login />} />
          <Route path='/contact' element={<Contact />} ></Route>
          <Route path='/cart' element={<Cart />}></Route>
          <Route path='/bmi' element={<BMICalculator />}></Route>
        </Routes>
      </Router>
    </>
  );
}

export default App;
